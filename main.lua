local gamestate = "menu"
local player = {x = 0, drawnX = 0, drawnY = 0}
local speed = 500
local bulletSpeed = 200
local bullets = {}
local asteroids = {}
local asteroidSpeed = 100
local spawnCount = 5
local spawnTimer = 0

local function drawGame()
	local w, h = love.graphics.getDimensions()

	-- Draw player
	player.drawnX = w/2 + player.x
	player.drawnY = h - 24
	love.graphics.rectangle("fill", player.drawnX - 16, player.drawnY, 32, 16)

	-- Draw bullets
	for _, bullet in ipairs(bullets) do
		love.graphics.circle("fill", bullet.x, bullet.y, 4)
	end

	-- Draw asteroids
	for _, asteroid in ipairs(asteroids) do
		love.graphics.circle("line", asteroid.x, asteroid.y, asteroid.size, 5)
	end
end

local function startGame()
	gamestate = "game"
	player.x = 0
	bullets = {}
	asteroids = {}
	spawnCount = 5
end

local function shoot(x, y)
	local bullet = {x = x, y = y}
	bullets[#bullets+1] = bullet
	return bullet
end

local function drawText(text, y)
	local w, h = love.graphics.getDimensions()
	local font = love.graphics.getFont()
	local tw = font:getWidth(text)
	love.graphics.print(text, math.floor((w-tw)*0.5), y)
end

function love.draw()
	if gamestate == "menu" then
		drawText("Space Invaders", 20)
		drawText("Press enter to start", 100)
		drawText("Press escape to exit", 140)
	elseif gamestate == "game" then
		drawGame()
	end
end

local function distance(a, b)
	local dx, dy = a.x-b.x, a.y-b.y
	return math.sqrt(dx*dx + dy*dy)
end

local function remove(tbl, toRemove)
	local newTbl = {}
	for i, item in ipairs(tbl) do
		if not toRemove[i] then
			newTbl[#newTbl+1] = item
		end
	end
	return newTbl
end

local function detectCollisions()
	local removeBullets = {}
	local removeAsteroids = {}

	for bulletIndex, bullet in ipairs(bullets) do
		for asteroidIndex, asteroid in ipairs(asteroids) do
			local d = distance(bullet, asteroid)
			if d <= asteroid.size+6 then
				removeBullets[bulletIndex] = true
				removeAsteroids[asteroidIndex] = true
			end
		end
	end

	bullets = remove(bullets, removeBullets)
	asteroids = remove(asteroids, removeAsteroids)
end

local function spawnAsteroid()
	local w = love.graphics.getWidth()
	local asteroid = {
		x = math.random(0, w),
		y = 0,
		size = math.random(16, 32)
	}
	asteroids[#asteroids+1] = asteroid
	return asteroid
end

function love.update(dt)
	if gamestate == "game" then
		local w, h = love.graphics.getDimensions()
		-- Move player
		if love.keyboard.isDown("left") then
			player.x = player.x - speed * dt
		elseif love.keyboard.isDown("right") then
			player.x = player.x + speed * dt
		end

		-- Move bullets
		local newBullets = {}
		for _, bullet in ipairs(bullets) do
			bullet.y = bullet.y - bulletSpeed * dt
			if bullet.y > -8 then
				newBullets[#newBullets+1] = bullet
			end
		end
		bullets = newBullets

		-- Move asteroids
		local newAsteroids = {}
		for _, asteroid in ipairs(asteroids) do
			asteroid.y = asteroid.y + asteroidSpeed * dt
			if asteroid.y < h + 32 then
				newAsteroids[#newAsteroids+1] = asteroid
			end
		end
		asteroids = newAsteroids

		detectCollisions()

		spawnTimer = spawnTimer + dt

		if spawnTimer >= spawnCount then
			spawnTimer = 0
			spawnAsteroid()
			spawnCount = spawnCount * 0.9
			print("Spawn delay is " .. spawnCount)
		end
	end
end

function love.keypressed(key)
	if gamestate == "menu" then
		if key == "escape" then
			love.event.quit()
		elseif key == "return" then
			startGame()
		end
	elseif gamestate == "game" then
		if key == "escape" then
			gamestate = "menu"
		elseif key == "space" then
			shoot(player.drawnX, player.drawnY)
		end
	end
end

function love.load()
	love.window.setTitle("Space Invaders")
	love.graphics.setFont(love.graphics.newFont(42))
end
