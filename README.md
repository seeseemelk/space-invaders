# Space Invaders
A very simple space invaders clone made for a 1-hour hackathon.

## How to run
Download and install Love2D, then drag-and-drop the source directory onto `love.exe`.
